.DEFAULT_GOAL := help

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

##
##Helpers
##---------------------------------------------------------------------------

build:	  ## --> Builds the image.
	bash -c "cd src/ && packer build ."
.PHONY: build

##---------------------------------------------------------------------------
